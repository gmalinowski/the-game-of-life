import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

/**
 * TheGameOfLife
 * Created by Grzegorz Malinowski on 2017-05-09, 19:03.
 * gmalinowski@protonmail.com
 */
public class Cell {
    private Rectangle cell;
    private boolean alive = false;
    private int aliveNeighbors = 0;
    private ArrayList<Integer> neighbors = new ArrayList<Integer>();


    public Cell(int width, int height) {
        cell = new Rectangle();
        cell.setWidth(width);
        cell.setHeight(height);
        cell.setTranslateX(0);
        cell.setTranslateY(0);
        cell.setFill(Color.BLACK);
    }

    void setNextTo(Cell cell) {
        int x = cell.getX() + cell.getWidth();
        int y = cell.getY();

        this.cell.setTranslateX(x);
        this.cell.setTranslateY(y);
    }

    boolean isAlive() {
        return alive;
    }

    void kill() {
        alive = false;
        cell.setFill(Color.BLACK);
    }

    void bringToLife() {
        alive = true;
        cell.setFill(Color.CHARTREUSE);
    }

    int getX() {
        return ((int) cell.getTranslateX());
    }

    int getY() {
        return ((int) cell.getTranslateY());
    }

    int getWidth() {
        return ((int) cell.getWidth());
    }

    int getHeight() {
        return ((int) cell.getHeight());
    }

    void setXY(int x, int y) {
        cell.setTranslateX(x);
        cell.setTranslateY(y);
    }

    Rectangle getCell() {
        return cell;
    }

    void addNeighbor(int index) {
        neighbors.add(index);
    }

    ArrayList<Integer> getNeighbors() {
        return neighbors;
    }

    void setNumAliveNeighbors(int num) {
        aliveNeighbors = num;
        refresh();
    }

    void refresh() {
        if (alive) {
            if (aliveNeighbors != 2 && aliveNeighbors != 3)
                kill();
        } else {
            if (aliveNeighbors == 3)
                bringToLife();
        }
    }

}
