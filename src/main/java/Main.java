import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * TheGameOfLife
 * Created by Grzegorz Malinowski on 2017-05-09, 18:58.
 * gmalinowski@protonmail.com
 */
public class Main extends Application {
    List<Cell> cells = new ArrayList<Cell>();
    private int width = 1000, height = 1000, cellWidth = 10, counter = 1000, fps = 30, howManyAlive = 700;
    private int rows = height/cellWidth;
    private int columns = width/cellWidth;
    private int numCells = rows*columns;

    private Scene createScene() {
        Pane root = new Pane();
        setCellsOnPane(root);
        setNeighbors();
        setRandLife(howManyAlive);

        AnimationTimer at = new AnimationTimer() {
            @Override
            public void handle(long now) {
                counter++;

                if (counter == fps) {
                    counter = 0;
                    setAllAliveNeighbors();
                }
            }
        };
        at.start();

        return new Scene(root, width, height);
    }

    private void setAllAliveNeighbors() {
        for (Cell cell: cells) {
            int sum = 0;
            for (int i = 0; i < cell.getNeighbors().size(); i++) {
                if (cells.get(cell.getNeighbors().get(i)).isAlive())
                    sum++;
            }
            System.out.println(sum);
            cell.setNumAliveNeighbors(sum);
        }
    }

    private void setNeighbors() {
        for (int i = 0; i < cells.size(); i++) {

            //left
            if (i % columns != 0) {
                cells.get(i).addNeighbor(i - 1);
                //left up
                if (i >= columns)
                    cells.get(i).addNeighbor((i - 1) - columns);
                //left down
                if (i < ((cells.size()) - columns))
                    cells.get(i).addNeighbor((i + columns) - 1);
            }
            //right
            if ((i < cells.size() - 1) && (i + 1) % columns != 0) {
                cells.get(i).addNeighbor(i + 1);
                //right up
                if (i >= columns)
                    cells.get(i).addNeighbor(i + 1 - columns);
                //right down
                if (i <= (cells.size() - 1) - columns)
                    cells.get(i).addNeighbor(i + 1 + columns);
            }
            //up
            if (i >= columns)
                cells.get(i).addNeighbor(i - columns);
            //down
            if (i <= ((cells.size() - 1) - columns)) {
                cells.get(i).addNeighbor(i + columns);
            }
        }
    }

    private void setRandLife(int howMany) {
        int max = (numCells);
        Random gen = new Random();

        for (int i = 0; i < howMany; i++) {
            cells.get(gen.nextInt(max)).bringToLife();
        }
    }

    private void setCellsOnPane(Pane root) {
        cells.add(new Cell(cellWidth, cellWidth));
        int howMany = (numCells);
        for (int i = 1; i < howMany; i++) {
            cells.add(new Cell(cellWidth, cellWidth));

            cells.get(i).setNextTo(cells.get(i - 1));

            if (i % (rows) == 0) {
                cells.get(i).setXY(0, cells.get(i).getY() + cellWidth);
            }
        }

        for (Cell cell:
                cells) {
            root.getChildren().add(cell.getCell());
        }
    }

    public void start(Stage primaryStage) throws Exception {
        Scene scene = createScene();

        scene.setOnKeyPressed(event -> {
            counter = 0;
        });

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
